import { defineComponent, ref } from 'vue';

export default defineComponent({
  name: 'logo',
  setup() {
    const isShow = ref(true);
    
    const showHideMenu = () => {
      isShow.value = !isShow.value;
      window.dispatchEvent(new CustomEvent('hideShowMenu', {
        detail: {
          answer: isShow.value
        }
      }));
    }
    
    return {
      showHideMenu
    }
  }
});